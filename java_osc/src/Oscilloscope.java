import jssc.SerialPortException;

public class Oscilloscope
{
	private SerialPortCommunication UART = new SerialPortCommunication();
	private byte[] data = new byte[128];
	private int[] intData = new int[128];
	private int[] voltage = new int[64];
	private double vScale = 0.5;
	private double tScale = 20.0;
	private int nscale = 6;
	private int wscale = 5;
	public int loopCnt = 0;
	private double uMax = 0.0;
	private double uMin = 0.0;
	private double RMS = 0.0;
	private double average = 0;
	
	public double[] voltBuffer = new double[32768];
	public boolean isOn = false;
	public boolean connection = false;
		
	int z = 0;
public void setOn(boolean on)
{
	isOn = on;
}
public void openConnection(boolean status)
{
	if(status) {
		try {			
			UART.openConnection();
			UART.setParameters();
			connection = true;
		} catch (SerialPortException e) {
			System.out.println(e);
		}
	}else {
		try {
			UART.closeConnection();
			connection = false;
		} catch (SerialPortException e) {
			System.out.println(e);
		}
	}	
}
public void receiveData()
{
	data = UART.receive();
}
public void decodeData()
{	
	receiveData();
	
	int off = 0;
	int pp = 0;
	int pn = 0;
	for(int i=0;i<((data.length/2)-1);i++) {
		if(data[2*i]!=data[2*i+2]) {
			pp+=1;
		}
		if(data[2*i+1]!=data[2*i+3]) {
			pn+=1;
		}
	}
	if(pp>pn) {
		off = 1;
	}
	
	for(int i=0;i<data.length/2;i++) {
		if(i==0) {
			if(data[2*i]<0) {
				intData[2*i]=(int)data[2*i]+256;
			}else {
				intData[2*i]=(int)data[2*i];
			}
			if(data[2*i+1]<0) {
				intData[2*i+1-off]=(int)data[2*i+1]+256;
			}else {
				intData[2*i+1-off]=(int)data[2*i+1];
			}
		}else{
			if(data[2*i]<0) {
				intData[2*i-off]=(int)data[2*i]+256;
			}else {
				intData[2*i-off]=(int)data[2*i];
			}
			if(data[2*i+1]<0) {
				intData[2*i+1-off]=(int)data[2*i+1]+256;
			}else {
				intData[2*i+1-off]=(int)data[2*i+1];
			}
		}
	}	
	
	if(off==1) {
		for(int i=0;i<3;i++) {
			intData[i]=intData[i+4];
		}
		intData[data.length-1]=intData[data.length-3];
	}
	

	for(int j=0;j<intData.length/2;j++) {
		if(intData[2*j]==128) {
			intData[2*j]=0;
		}
	}
	
	for(int k=0;k<voltage.length;k++) {
		voltage[k]=(intData[k*2]<<8) + intData[k*2+1];
	}
}
public void fillBuffer() 
{
	decodeData();
	for(int i=0;i<voltBuffer.length-voltage.length;i++) {
		voltBuffer[i]=voltBuffer[i+voltage.length];
	}
	
	for(int i=0;i<voltage.length;i++) {
		voltBuffer[(voltBuffer.length-voltage.length)+i] = voltage[i];
		
		
		/*voltBuffer[voltBuffer.length-1-i] = (2048.0+Math.random()*10+1250.0*Math.sin(2.0*Math.PI*78.0*(double)(i+loopCnt*voltage.length)/5000.0));*/		  //test data
		
		/*if(z%500<250) {																																  //test data, set #2
			voltBuffer[(voltBuffer.length-voltage.length)+i] = (2040.0+1250+Math.random()*10);
		}else {
			voltBuffer[(voltBuffer.length-voltage.length)+i] = (2040.0-1250+Math.random()*20);
		}
		z++;*/
	}	
}
public double[] getData()
{
	fillBuffer();
	return voltBuffer;	
}
public void findExtremes()
{
	uMin=4095.0;
	uMax=0.0;
	for(int i=0;i<voltBuffer.length;i++) {
		if(voltBuffer[i]>uMax) {
			uMax=voltBuffer[i];
		}
		if(voltBuffer[i]<uMin) {
			uMin=voltBuffer[i];
		}
	}
	uMin=Math.round((uMin-2048.0)*0.0806)/100.0;
	uMax=Math.round((uMax-2048.0)*0.0806)/100.0;
}
public void findRMS()
{
	double sumSQ = 0.0;
	double temp = 0.0;
	for(int i=0;i<voltBuffer.length;i++) {
		temp=(voltBuffer[i]-2048)*0.000806;
		sumSQ+=(temp*temp);
	}
	RMS = Math.sqrt(sumSQ/(voltBuffer.length));
}
public void findAverage() 
{
	double temp=0;
	for(int i=0;i<voltBuffer.length;i++) {
		temp += (voltBuffer[i]-2048)*0.000806;
	}
	average = temp/voltBuffer.length;
}
public double getRMS()
{
	RMS = Math.round(RMS*100.0)/100.0;
	return RMS;
}
public double getAmplitude() 
{
	double amplitude = 0;
	double minMod = Math.abs(uMin);
	double maxMod = Math.abs(uMax);
	if(minMod>maxMod) {
		amplitude = Math.round(100.0*minMod)/100.0;
	}else {
		amplitude = Math.round(100.0*maxMod)/100.0;
	}
	return amplitude;
}
public double getPP() 
{
	double PP = 0;
	PP=Math.round((uMax-uMin)*100.0)/100.0;
	return PP;
}

public double getUmax()
{
	return Math.round(uMax*100.0)/100.0;
}
public double getUmin()
{	
	return Math.round(uMin*100.0)/100.0;
}
public double getAverage()
{
	average = Math.round(average*100.0)/100.0;
	return average;
}
public void scaleWidth(boolean scale)
{
	if(scale) {
		wscale++;
	}else {
		wscale--;
	}
	
	switch(wscale) {
	case 0:
		wscale = 1;
		break;
	case 1:
		tScale = 1.0;
		break;
	case 2:
		tScale = 2.0;
		break;
	case 3:
		tScale = 5.0;
		break;
	case 4:
		tScale = 10.0;
		break;
	case 5:
		tScale = 20.0;
		break;
	case 6:
		tScale = 50.0;
		break;
	case 7:
		wscale = 6;
		break;
	
	}
}
public void scaleHeight(boolean scale)
{

	if(scale) {
		nscale++;
	}else {
		nscale--;
	}
	
	switch(nscale) {
	case 0:
		nscale = 1;
		break;
	case 1:
		vScale = 0.01;
		break;
	case 2:
		vScale = 0.02;
		break;
	case 3:
		vScale = 0.05;
		break;
	case 4:
		vScale = 0.1;
		break;
	case 5:
		vScale = 0.2;
		break;
	case 6:
		vScale = 0.5;
		break;
	case 7:
		vScale = 1;
		break;
	case 8:
		nscale = 7;
		break;
	}
}
public double getWidthScale()
{
	return tScale;
}
public double getHeightScale()
{
	return vScale;
}
}
