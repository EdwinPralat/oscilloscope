//import java.util.Arrays;
import jssc.*;

public class SerialPortCommunication //implements SerialPortEventListener,CommPortOwnershipListener
{
	private String[] portNames;
	private int baudRate = 115200;
	private int databits = 8;
	private int stopbits = 1;
	private int parity = SerialPort.PARITY_NONE;
	private int flowControlIn;
	private int flowControlOut;
	private byte[] received = new byte[128];
	private String portName = "COM5";
	
	SerialPort serialPort = new SerialPort(portName);
	
public byte[] receive()
{	
	try {                   
		received = serialPort.readBytes(128);		
	} catch (SerialPortException e) {
		System.out.println(e);
	}
	return received;
}
public byte[] exampleData() {
	
	byte[] exData = new byte[128];	
	for(int i=0;i<32;i++) {
		exData[4*i]=(byte)4;
		exData[4*i+1]=(byte)10;
		exData[4*i+2]=(byte)4;
		exData[4*i+3]=(byte)14;
	}			
	return exData;
}
public void scanPorts()
{
	portNames = SerialPortList.getPortNames();
}
public void openConnection() throws SerialPortException
{
	serialPort.openPort();
}
public void closeConnection() throws SerialPortException
{
	serialPort.closePort();
}
public void setParameters() throws SerialPortException
{
	serialPort.setParams(baudRate, databits, stopbits, SerialPort.PARITY_NONE);
	int mask = SerialPort.MASK_RXCHAR + SerialPort.MASK_CTS + SerialPort.MASK_DSR;
	serialPort.setEventsMask(mask);
}
public void setPort(String newPort)
{
	portName = newPort;	
}
public void setBaudRate(int newBaudRate)
{
	baudRate = newBaudRate;
}
public void setDatabits(int newDatabits) 
{
	databits = newDatabits;
}
public void setStopbits(int newStopbits) 
{
	stopbits = newStopbits;
}
public void setParity(int newParity) 
{
	parity = newParity;
}
public void setFlowControlIn(int newFlowControlIn) 
{
	flowControlIn = newFlowControlIn;
}
public void setFlowControlOut(int newFlowControlOut) 
{
	flowControlOut = newFlowControlOut;
}
public String[] getPortNames()
{
	scanPorts();
	return portNames;
}
public int getBaudRate()
{
	return baudRate;
}
public int getFlowControlIn()
{
	return flowControlIn;
}
public int getFlowControlOut()
{
	return flowControlOut;
}
public int getDatabits()
{
	return databits;
}
public int getStopBits()
{
	return stopbits;
}
public int getParity()
{
	return parity;
}
}