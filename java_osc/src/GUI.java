import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.Arrays;

import javax.swing.*;

public class GUI extends JFrame implements ActionListener{
	
	private static final long serialVersionUID = -679625735377797536L;

    private Graph graph = new Graph();
    private Oscilloscope oscil = new Oscilloscope();
    private SerialPortCommunication UART = new SerialPortCommunication();
    private FFT fft = new FFT();
	private JFrame frame = new JFrame("System for measuring the electrical waveforms");
    	
	private JButton startButton = new JButton("Start/Stop");
	private JButton connectButton = new JButton("Connect");
	private JToggleButton FFTButton = new JToggleButton("FFT mode");
	private JButton upscaleHeightButton = new JButton("H. scale +");
	private JButton downscaleHeightButton = new JButton("H. scale -");
	private JButton upscaleWidthButton = new JButton("W. scale +");
	private JButton downscaleWidthButton = new JButton("W. scale -");
	private JButton resetButton = new JButton("Reset");

	private JTextField showPorts = new JTextField(" Available ports: " + Arrays.toString(UART.getPortNames()));
	private JTextField showWidthScale = new JTextField(" t = " + oscil.getWidthScale() + "ms");
	private JTextField showHeightScale = new JTextField(" U = " + oscil.getHeightScale() + "V");
	private JTextField showUmax = new JTextField(" Umax = " + oscil.getUmax() + "V");
	private JTextField showUmin = new JTextField(" Umin = " + oscil.getUmin() + "V");
	private JTextField showAmplitude = new JTextField (" A = " + oscil.getAmplitude() + "V");
	private JTextField showPeakPeak = new JTextField(" Up-p = " + oscil.getPP() + "V");
	private JTextField showRMS = new JTextField(" RMS = " + oscil.getRMS() + "V");
	private JTextField showAverage = new JTextField(" AVG = " + oscil.getAverage() + "V");

	private int fwidth = 1440;
    private int fheight = 810;
	private Container element = getContentPane();			
    private Rectangle boundaries = frame.getBounds(); 
    
    private int xoffset = oscil.voltBuffer.length-(int)(275*oscil.getWidthScale());
	private int[] xAxisData = new int[graph.getWidth()];
	private double[] yAxisData = new double[graph.getWidth()];
	
@Override
public void actionPerformed(ActionEvent e) {
	if(e.getSource() == startButton & oscil.isOn==false) {
		if(oscil.connection==true) {
			oscil.setOn(true);
			startThread();
			System.out.println("on = " + oscil.isOn);
		}else {
			System.out.println("Port is not opened");
		}
	}else if(e.getSource()==startButton & oscil.isOn == true) {
		oscil.setOn(false);
		System.out.println("on = " + oscil.isOn);
	}
	if(e.getSource() == connectButton & oscil.connection==false) {
		oscil.openConnection(true);
		System.out.println("connected = " + oscil.connection);
	}else if(e.getSource() == connectButton & oscil.connection==true) {
		if(oscil.isOn) {
			oscil.setOn(false);
		}
		oscil.openConnection(false);
		System.out.println("connected = " + oscil.connection);
	}
	if(e.getSource() == upscaleHeightButton) {
		oscil.scaleHeight(false);
	}
	if(e.getSource() == downscaleHeightButton) {
		oscil.scaleHeight(true);
	}
	if(e.getSource() == upscaleWidthButton) {
		oscil.scaleWidth(false);
	}
	if(e.getSource() == downscaleWidthButton) {
		oscil.scaleWidth(true);
	}
	if(e.getSource()==resetButton) {
		for(int i=0;i<oscil.voltBuffer.length;i++) {
			oscil.voltBuffer[i]=0.0;
		}
	}
	if(e.getSource()==FFTButton) {
		/*update*/
	}
	update();
}

public void update()
{
	showPorts.setText(" Available ports: " + Arrays.toString(UART.getPortNames()));
	showWidthScale.setText(" t = " + oscil.getWidthScale() + "ms");
	showHeightScale.setText(" U = " + oscil.getHeightScale() + "V");
	showUmax.setText(" Umax = " + oscil.getUmax() + "V");
	showUmin.setText(" Umin = " + oscil.getUmin() + "V");
	showAmplitude.setText(" A = " + oscil.getAmplitude() + "V");
	showPeakPeak.setText(" Up-p = " + oscil.getPP() + "V");
	showRMS.setText(" RMS = " + oscil.getRMS() + "V");
	showAverage.setText(" AVG = " + oscil.getAverage() + "V");
	graph.setData(xAxisData, yAxisData);
	graph.repaint();		
}
public void startThread() 
{
	@SuppressWarnings({ "unchecked", "rawtypes" })
	SwingWorker worker = new SwingWorker(){
		
		@Override
		protected String doInBackground() throws Exception{
				while(oscil.isOn==true) {					
					oscil.getData();
					if(FFTButton.isSelected()){
						for(int i=0;i<xAxisData.length;i++) {
							xAxisData[i] = i;			
							//yAxisData[i] = FFT.;
						}
					} else {
						for(int i=0;i<xAxisData.length;i++) {
							xAxisData[i] = i;			
							yAxisData[i] = oscil.voltBuffer[(int)(i*100*oscil.getWidthScale()/graph.getWidth())+xoffset];
						}
					}
				publish(xAxisData);
				publish(yAxisData);
				process();			
			}
			return "Execution stopped";
		}
		
		protected void process() {
			for(int i=0;i<xAxisData.length;i++) {
				yAxisData[i] = ((graph.getHeight()*0.0000806)/(oscil.getHeightScale()))*(yAxisData[i]-2048);
			}
			oscil.findExtremes();
			oscil.findRMS();
			oscil.findAverage();
			update();
		}
			
		@Override
		protected void done() {
			System.out.println("Background process ended");
		}		
	};
	worker.execute();
}
public GUI()
{
	update();
	
	frame.setSize(new Dimension(fwidth,fheight));
	frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
	
	startButton.addActionListener(this);	
	element.add(startButton);
	
	connectButton.addActionListener(this);
	element.add(connectButton);
		
	FFTButton.addActionListener(this);
	//element.add(FFTButton);

	graph.addActionListener(this);
	element.add(graph);	
	
	upscaleHeightButton.addActionListener(this);
	element.add(upscaleHeightButton);
	
	downscaleHeightButton.addActionListener(this);
	element.add(downscaleHeightButton);	
	
	upscaleWidthButton.addActionListener(this);
	element.add(upscaleWidthButton);
	
	downscaleWidthButton.addActionListener(this);
	element.add(downscaleWidthButton);
	
	resetButton.addActionListener(this);
	element.add(resetButton);
	
	element.add(showPorts);	
	element.add(showWidthScale);
	element.add(showHeightScale);	
	element.add(showUmin);
	element.add(showUmax);
	element.add(showAmplitude);
	element.add(showPeakPeak);
	element.add(showRMS);
	element.add(showAverage);

	frame.addComponentListener(new ComponentAdapter() {
		public void componentResized(ComponentEvent res) {
			boundaries = frame.getBounds();
			int wid = w(100);
			int hei = h(50);
			
			startButton.setBounds(10,10,wid,hei);
			connectButton.setBounds(20+wid,10,wid,hei);
			showPorts.setBounds(30+2*wid,10,2*wid,hei);
			FFTButton.setBounds(40+4*wid,10,wid,hei);
			downscaleHeightButton.setBounds(w(fwidth)-5*wid-70,10,wid,hei);
			upscaleHeightButton.setBounds(w(fwidth)-4*wid-60,10,wid,hei);
			downscaleWidthButton.setBounds(w(fwidth)-3*wid-50,10,wid,hei);
			upscaleWidthButton.setBounds(w(fwidth)-2*wid-40,10,wid,hei);
			resetButton.setBounds(w(fwidth)-wid-30,10,wid,hei);
			graph.setBounds(10,20+hei,w(fwidth)-40,h(fheight*11/14));
			showHeightScale.setBounds(10,h(fheight*11/14+60)+20,wid,hei);
			showWidthScale.setBounds(20+wid,h(fheight*11/14+60)+20,wid,hei);
			showUmin.setBounds(40+3*wid,h(fheight*11/14+60)+20,wid,hei);
			showUmax.setBounds(50+4*wid,h(fheight*11/14+60)+20,wid,hei);
			showAmplitude.setBounds(60+5*wid,h(fheight*11/14+60)+20,wid,hei);
			showPeakPeak.setBounds(70+6*wid,h(fheight*11/14+60)+20,wid,hei);
			showRMS.setBounds(80+7*wid,h(fheight*11/14+60)+20,wid,hei);
			showAverage.setBounds(90+8*wid,h(fheight*11/14+60)+20,wid,hei);
			
			xAxisData = new int[boundaries.width];
			yAxisData = new double[boundaries.width];			
		}
	});
	
    frame.setContentPane(element);
    frame.setMinimumSize(new Dimension(fwidth,fheight));
    frame.setVisible(true);
}
public int w(int resWidth)
{
    return (boundaries.width*resWidth/fwidth); 
}
public int h(int resHeight)
{
    return (boundaries.height*resHeight/fheight);
}

public static void main(String[] args)
{
	EventQueue.invokeLater(new Runnable() {
        public void run() {
            try {
                new GUI();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    });
}
}
