import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.*;

import java.awt.geom.*;

public class Graph extends JComponent implements MouseListener, MouseMotionListener
{
	private static final long serialVersionUID = 4825861898538618714L;
	
	private ActionListener al;
	
	private int[] xaxisData = new int[1];
	private double[] yAxisData = new double[1];
	private int x1;
	private int x2;
	private int y1;
	private int y2;
	
	private Color background;
	private Color lines;
	private Color chan1;
	private boolean pressed = false;
	
	private int x = 0;
	private int y = 0;
	private int startXPos = 0;
	private int startYPos = 0;
	public int xPosChange = 0;
	public int yPosChange = 0;
	
	public Graph() 
	{
        background = new Color(0x00,0x00,0x00);
        lines = new Color(0xAA,0xAA,0xAA);
        chan1 = new Color(0xff,0x57,0x33);
        addMouseListener(this);
        addMouseMotionListener(this);
	}
	
	public void setData(int xdata[], double yData[]) 
	{
		xaxisData = new int[xdata.length];
		yAxisData = new double[yData.length];
		for (int i=0;i<xdata.length;i++) {
			xaxisData[i] = xdata[i];
			yAxisData[i] = yData[i];
		}
	}    
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        Graphics2D g1=(Graphics2D)g;
        int width=getWidth();
        int height=getHeight();
        g1.setColor(background);
        g1.fillRect(0, 0, width, height);
        g1.setPaint(lines);
        for(int i=0;i<21;i++) {
        	if(i==20) {
        		g1.draw(new Line2D.Double(i*width/20,0,(i*width)/20,height));
        	}else{
        		g1.draw(new Line2D.Double(i*width/20,0,(i*width)/20,height));
        	}
    	}
        
        for(int i=0;i<11;i++) {
        	if(i==10) {
        		g1.draw(new Line2D.Double(0,(i*height)/10-1,width,(i*height)/10-1));
        	}else {
        		g1.draw(new Line2D.Double(0,(i*height)/10,width,(i*height)/10));
        	}
        }
        g1.setColor(chan1);
        for(int i=0;i<xaxisData.length-1;i++) {
        	x1=(int)(xaxisData[i]);
        	x2=(int)(xaxisData[i+1]); 
        	y1=-(int)(yAxisData[i]);
        	y2=-(int)(yAxisData[i+1]);
        	g1.drawLine(x1, y1+height/2, x2, y2+height/2);
        }
    }
    public void addActionListener(ActionListener pl)
    {
    	al=AWTEventMulticaster.add(al, pl);
    }
    public void removeActionListener(ActionListener pl)
    {
    	al=AWTEventMulticaster.add(al, pl);
    }
    public void position(MouseEvent e) {
    	Point p = e.getPoint();
    	x=p.x;
    	y=p.y; 
    	xPosChange = startXPos-x;
    	yPosChange = startYPos-y;
    	if(al!=null){
            al.actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "graph"));
        }
    }	
	@Override
	public void mouseDragged(MouseEvent e) 
	{
		if(pressed) {
			position(e);
		}
	}
	@Override
	public void mousePressed(MouseEvent e) 
	{
		pressed = true;	
		position(e);
		startXPos = x;
		startYPos = y;
	}
	@Override
	public void mouseReleased(MouseEvent e) 
	{
		pressed = false;
	}   
	@Override
	public void mouseClicked(MouseEvent e) 
	{
    	//do nothing	
	}
	@Override
	public void mouseMoved(MouseEvent e) 
	{
		//do nothing	
		
	}
	@Override
	public void mouseEntered(MouseEvent e) 
	{
		//do nothing	
	}

	@Override
	public void mouseExited(MouseEvent e) 
	{
		//do nothing	
	}
}
