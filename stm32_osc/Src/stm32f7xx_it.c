#include "main.h"
#include "stm32f7xx_it.h"

extern DMA_HandleTypeDef hdma_adc1;
extern TIM_HandleTypeDef htim10;
extern UART_HandleTypeDef huart4;

void NMI_Handler(void)
{
  
}


void HardFault_Handler(void)
{
 
  while (1)
  {
   
  }
}


void MemManage_Handler(void)
{
 while (1)
  {
		
  }	
}

void BusFault_Handler(void)
{
 
  while (1)
  {

  }
}

void UsageFault_Handler(void)
{
 while (1)
  {

  }
}


void SVC_Handler(void)
{

}

void DebugMon_Handler(void)
{
	
}


void PendSV_Handler(void)
{

}

void SysTick_Handler(void)
{

  HAL_IncTick();

}

void TIM1_UP_TIM10_IRQHandler(void)
{

  HAL_TIM_IRQHandler(&htim10);
}


void UART4_IRQHandler(void)
{

  HAL_UART_IRQHandler(&huart4);

}
void DMA2_Stream0_IRQHandler(void)
{

  HAL_DMA_IRQHandler(&hdma_adc1);

}
