# System for measuring the electrical waveforms

Project developed as a part of engineering thesis

## Table of contents

* [General info](#general-info)
* [Technologies](#technologies)
* [Features](#features)
* [Interface](#interface)
* [Status](#status)

## General info

The designed system measures the signal and calculates its parameters such as RMS, average, 
or peak-peak voltage values and presents it to the user in a manner similar to an oscilloscope. 

## Technologies

Project is created with:

* NUCLEO-F767ZI,
* Hardware Abstraction Layer,
* Java-Simple-Serial-Connector library,
* Java Swing toolkit.

## Features

* 5kHz sampling frequency (possible to increase)
* UART communication for data transfer
* Manual vertical and horizontal scaling
* Drawing the measured signal on a graph in a continuous manner
* Signal parameters calculation: RMS, average, peak-peak, min and max voltages and amplitude

## Interface

![plot](./graphics/triangle.png?raw=true "Interface")

*  1 - Button which starts or stops data acquiring process
*  2 - Button connecting to a chosen port - COM5 by default
*  3 - Available ports list
*  4 - Horizontal scale decrease button
*  5 - Horizontal scale increase button
*  6 - Vertical scale decrease button
*  7 - Vertical scale increase button
*  8 - Acquired data buffer-clearing button
*  9 - Graph
* 10 - Vertical scale
* 11 - Horizontal scale
* 12 - Maximal voltage
* 13 - Minimal voltage
* 14 - Amplitude
* 15 - Peak-peak voltage
* 16 - RMS voltage
* 17 - Average voltage

## Status

The project is still being worked on, besides features included in [Features](#features) section there are plans on adding:

* Spectral analysis function (work in progress)
* Frequency and period calculation function
* Trigger function 
* Autoset function
* Scrolling - both horizontal and vertical
* Markers usage
